// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_ProjectGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_ProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_ProjectGameMode();
};



