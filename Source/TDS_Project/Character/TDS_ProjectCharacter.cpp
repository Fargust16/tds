// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ProjectCharacter.h"

#include <string>

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDS_ProjectCharacter::ATDS_ProjectCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDS_ProjectCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);

			// FVector CursorLocation = CursorToWorld->GetComponentLocation();
			// GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Code %s"), *CursorLocation.ToString()));
		}
	}
}

void ATDS_ProjectCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_ProjectCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_ProjectCharacter::InputAxisY);
}

void ATDS_ProjectCharacter::AddCharacterMovementInput(FVector CursorLocation)
{
	if (!bIsSprinting)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	}
	else
	{
		FVector CharLocation = GetActorLocation();
		float CharX = CursorLocation.X - CharLocation.X;
		float CharY = CursorLocation.Y - CharLocation.Y;
			
		CharX = abs(CharX) > 25 ? CharX / abs(CharX) : 0;
		CharY = abs(CharY) > 25 ? CharY / abs(CharY) : 0;
			
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), CharX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), CharY);
	}
}

void ATDS_ProjectCharacter::InputAxisX(float AxisValue)
{
	AxisX = AxisValue;
}

void ATDS_ProjectCharacter::InputAxisY(float AxisValue)
{
	AxisY = AxisValue;
}

void ATDS_ProjectCharacter::MovementTick(float DeltaTime)
{
	
	if (const APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		// FHitResult HitResult;
		// PC->GetHitResultUnderCursor(ECC_Visibility, true, HitResult);
		
		float X, Y;
		FVector WorldLocation, WorldDirection;
		PC->GetMousePosition(X, Y);
		PC->DeprojectScreenPositionToWorld(X, Y, WorldLocation, WorldDirection);

		FVector ND = (WorldLocation.Z / WorldDirection.Z) * WorldDirection * -1;
		FVector CursorLocation = WorldLocation + ND;

		FRotator LookRotation = UKismetMathLibrary::FindLookAtRotation(
			GetActorLocation(),
			FVector(CursorLocation.X, CursorLocation.Y, 0.0f)
		);
		SetActorRotation(FRotator(0.0f, LookRotation.Yaw, 0.0f).Quaternion());
		
		AddCharacterMovementInput(CursorLocation);
	}
}

void ATDS_ProjectCharacter::CharacterStateUpdate()
{
	float ResSpeed = 600.0f;
	
	switch (MovementState)
	{
	case EMovementState::Walk_State :
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State :
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Aim_State :
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Sprint_State :
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	bIsSprinting = MovementState == EMovementState::Sprint_State;
}

void ATDS_ProjectCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterStateUpdate();
}

void ATDS_ProjectCharacter::ShowDebugText(FString DebugMessage)
{
	GEngine->AddOnScreenDebugMessage(
		-1,
		5.0f,
		FColor::Red,
		DebugMessage
	);
}
