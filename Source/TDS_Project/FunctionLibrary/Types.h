// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName="Aim_State"),
	Walk_State UMETA(DisplayName="Walk_State"),
	Run_State UMETA(DisplayName="Run_State"),
	Sprint_State UMETA(DisplayName="Sprint_State"),
};

USTRUCT(BlueprintType)
struct FCharState
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float AimSpeed = 300.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float WalkSpeed = 200.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float RunSpeed = 600.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float SprintSpeed = 800.0f;
};

UCLASS()
class TDS_PROJECT_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	
};
